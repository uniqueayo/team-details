# Team Details

This is a simple app that toggles team member details using VueJs

## Setup

- Clone this repo
- Open index file in a browser

## Image
![Screenshot_2019-08-08_at_9.39.14_PM](/uploads/c6a35d2d55fb6a2e6ec2c428f5472c27/Screenshot_2019-08-08_at_9.39.14_PM.png)